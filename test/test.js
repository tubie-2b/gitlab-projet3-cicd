const assert = require('assert');
const request = require('supertest');
const { app } = require('../app'); // Importez l'application express depuis votre fichier app.js

describe('Todo List App', function() {
  it('should return status 200 when accessing the homepage', function(done) {
    request(app)
      .get('/')
      .expect(200, done);
  });

  it('should display the list of tasks on the homepage', function(done) {
    request(app)
      .get('/')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        // Vérifiez ici si la liste des tâches est correctement affichée sur la page d'accueil
        done();
      });
  });

  it('should add a new task', function(done) {
    request(app)
      .post('/addtask')
      .send({ newtask: 'Test new task' })
      .expect(302) // Redirection après l'ajout d'une tâche
      .end(function(err, res) {
        if (err) return done(err);
        // Vérifiez ici si la nouvelle tâche est ajoutée avec succès
        done();
      });
  });

  it('should mark a task as complete', function(done) {
    request(app)
      .post('/removetask')
      .send({ check: 'Acheter des pommes' }) // Marquer une tâche existante comme complétée
      .expect(302) // Redirection après avoir marqué une tâche comme complétée
      .end(function(err, res) {
        if (err) return done(err);
        // Vérifiez ici si la tâche est correctement marquée comme complétée
        done();
      });
  });
});
